# ProductPackages
Created by: Phillip Smith
Email: kaldhu@yahoo.com

This site is currently being hosted at https://productsmicroservice.azurewebsites.net/.

uses 3rd party API "product-service" to get Product information, this is hosted at https://product-service.herokuapp.com/api/v1/products

The Api is accessed via 

```
    /api/product
        > get

    /apo/product/:productId
        > get
```
