﻿const fetch = require('node-fetch');
function productController() {

    //TODO: Cache product information
    async function getData(apiEndPoint){
        const password =  process.env.password
        const username =  process.env._username
        const apiAddress = process.env.apiAddress

        const response = await fetch(`${apiAddress}${apiEndPoint}`,
        {
            method: 'get',
            headers: {'Authorization': 'Basic ' + Buffer.from(username + ":" + password).toString('base64')}
        });
        console.log(response);
        return await response.json();
    }

    async function get(request, result) {
        const data = await getData();
        return result.json(data);
    }

    async function getById(request, result) {
        const data = await getData(`/${request.params.productId}`);
        return result.json(data);
        
    }
    
    async function getProductPrice(request, result) {
        const data = await getData(`/${request.params.productId}`);
        return result.json(data.usdPrice);
    }

    return { get, getById, getProductPrice};
}

module.exports = productController;