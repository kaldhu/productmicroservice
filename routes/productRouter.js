﻿const express = require('express');

const productPackage = express.Router();
const productController = require('../controller/productController');

function routes() {

    const controller = productController();

    productPackage.route('/product')
        .get(controller.get)


        productPackage.route('/product/:productId')
        .get(controller.getById)
        productPackage.route('/product/:productId/get_price')
        .get(controller.getProductPrice)

    return productPackage;
}

module.exports = routes;